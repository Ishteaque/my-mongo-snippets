from pymongo import MongoClient
from bson.objectid import ObjectId
from bson.son import SON  # 

client = MongoClient("localhost", 27017)

db = client.myDB

people = db.people 

## Insert One
#people.insert_one({"name": "Karim", "age": 22})
## Get Object Id while inserting
#person_id=people.insert_one({"name": "Karim", "age": 22, "insterests":["programming", "travelling", "math"]}).inserted_id
# print(person_id)

## Print All
for person in people.find():
	print(person)

## Print with Find and ObjectId
#print([p for p in people.find({"_id": ObjectId(person_id)})])

#print([p for p in people.find({"age": {"$lt": 31}})]) # check mongo DB Query Operators
#print(people.count_documents({"name": "Ishte"}))

## Update
#people.update_one({"_id": ObjectId("6607e9362ed8993d0c059de6")}, {"$set": {"age": 23}})

## Delete 
#people.delete_many({"age": {"$lt": 25}})


## Group By - name with age average

pipeline = [
	{
		"$group": {
			"_id": "$name",
			"averageAge": {"$avg": "$age"}
		}
	},
	{
		"$sort": SON([("averageAge", -1), ("_id", -1)])
	}
]

results = people.aggregate(pipeline)
for result in results:
	print(result)
